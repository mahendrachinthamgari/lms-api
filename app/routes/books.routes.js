module.exports = app => {
    const books = require("../controllers/books.controller.js");

    const router = require("express").Router();

    router.post("/", books.create);

    router.get("/", books.findAll);

    router.get("/:id", books.findOne);

    router.get("/genre/:id", books.findAllByGenre);

    router.put("/:id", books.update);

    app.use("/api/books", router);
};