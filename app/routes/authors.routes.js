module.exports = app => {
    const authors = require("../controllers/authors.controller");

    const router = require("express").Router();

    router.get("/", authors.findAll);

    router.get("/:id", authors.getAuthorBooks);

    app.use("/api/authors", router);
};