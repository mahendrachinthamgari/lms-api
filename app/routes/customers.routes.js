module.exports = app => {
    const customers = require("../controllers/customers.controller");

    const router = require("express").Router();

    router.get("/", customers.findAll);

    router.get("/:id", customers.getUserBooks);

    app.use("/api/customers", router);
};