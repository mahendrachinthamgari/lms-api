module.exports = (sequelize, Sequelize) => {
    const Authors = sequelize.define("authors", {
        authorId: {
            type: Sequelize.STRING,
            primaryKey: true
        },
        name: {
            type: Sequelize.STRING
        },
        location: {
            type: Sequelize.STRING
        },
        dob: {
            type: Sequelize.DATEONLY
        },
    }, {
        freezeTableName: true,
        timestamps: false,
        underscored: true
    });
    return Authors;
};