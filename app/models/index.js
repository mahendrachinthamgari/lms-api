const dbConfig = require("../config/db.config.js");
const Sequelize = require("sequelize");

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,

    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

// Relations (foreign key)

db.books = require("./books.model.js")(sequelize, Sequelize);
db.customers = require("./customers.model.js")(sequelize, Sequelize);
db.authors = require("./authors.model.js")(sequelize, Sequelize);

// 1 to Many Relation one customer to many books

db.customers.hasMany(db.books, {
    foreignKey: 'customer_id',
    as: 'books'
});

db.books.belongsTo(db.customers, {
    foreignKey: 'customer_id',
    as: 'customer'
});


// 1 to Many Realation one author to many books

db.authors.hasMany(db.books, {
    foreignKey: 'author_id',
    as: 'booksWritten'
});

db.books.belongsTo(db.authors, {
    foreignKey: 'author_id',
    as: 'author_details'
});


module.exports = db;