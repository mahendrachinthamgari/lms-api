module.exports = (sequelize, Sequelize) => {
    const Customers = sequelize.define("customers", {
        customerId: {
            type: Sequelize.STRING,
            primaryKey: true
        },
        name: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.STRING
        },
        phone: {
            type: Sequelize.STRING
        },
    }, {
        freezeTableName: true,
        timestamps: false,
        underscored: true
    });
    return Customers;
};