module.exports = (sequelize, Sequelize) => {
    const Books = sequelize.define("books", {
        title: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.STRING
        },
        price: {
            type: Sequelize.INTEGER
        },
        publisher: {
            type: Sequelize.STRING
        },
        genre: {
            type: Sequelize.STRING
        },
        isAvailable: {
            type: Sequelize.BOOLEAN
        },
    }, {
        freezeTableName: true,
        timestamps: false,
        underscored: true
    });
    return Books;
};
