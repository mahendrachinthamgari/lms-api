const db = require("../models");
const Authors = db.authors;
const Books = db.books;


const findAll = (req, res) => {

    Authors.findAll({})
        .then(data => {
            if (data.length > 0) {
                res.send(data);
            } else {
                res.status(404).send({
                    message: "No Authors found"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving Authors data."
            });
        });
};

// one to many realation

const getAuthorBooks = (req, res) => {
    const id = req.params.id;

    Authors.findOne({
        include: [{
            model: Books,
            as: 'booksWritten'
        }],
        where: { authorId: id }
    })
        .then((data) => {
            if (data) {
                res.send(data)
            } else {
                res.status(404).send({
                    message: "AuthorID doesn't exist"
                });
            }
        })
        .catch((err) => {
            res.status(500).send({
                message: "Error occured while retreiving the author."
            });
        });
}


module.exports = {
    findAll,
    getAuthorBooks,
}