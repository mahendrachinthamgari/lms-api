const db = require("../models");
const Books = db.books;


const create = (req, res) => {
    const title = req.body.title;
    console.log("hello")
    if (!title) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    } else {
        Books.findAll({
            where: {
                title: title
            }
        })
            .then(data => {
                if (data.length > 0) {
                    res.status(400).send({
                        message: "Book already exists."
                    })
                } else {
                    const books = {
                        title: req.body.title,
                        description: req.body.description,
                        price: req.body.price,
                        publisher: req.body.publisher,
                        genre: req.body.genre.toLowerCase(),
                        isAvailable: req.body.isAvailable === false ? req.body.isAvailable : true,
                        customer_id: req.body.customer_id,
                        author_id: req.body.author_id
                    };
                    if (books.isAvailable && books.customer_id) {
                        res.send({
                            message: "Please make isAvailable False"
                        });
                    } else {
                        Books.create(books)
                            .then(data => {
                                res.status(200).send({
                                    message: "Book added successfully"
                                });
                            })
                            .catch(err => {
                                res.status(500).send({
                                    message:
                                        err.message || "Some error occurred while adding the books."
                                });
                            });
                    }
                }
            })
            .catch((err) => {
                res.status(500).send({
                    message:
                        err.message || "Some error occurred while retrieving books."
                });
            });
    }
};

const findAll = (req, res) => {
    const query = req.query
    const temp = {}
    if (query.available !== undefined) {
        temp.isAvailable = query.available;
    }
    if (query.authorId) {
        temp.author_id = query.authorId;
    }

    // const query = req.query.available === undefined ? {} : {isAvailable: req.query.available};

    Books.findAll({
        where: temp
    })
        .then(data => {
            if (data.length > 0) {
                res.send(data);
            } else {
                res.status(404).send({
                    message: "No Books found"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving books."
            });
        });
};

const findOne = (req, res) => {
    const id = req.params.id;

    Books.findByPk(id)
        .then(data => {

            if (data) {
                res.send(data);
            } else {
                res.status(404).send({
                    message: `No book found with id ${id}`
                })
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving book with id=" + id
            });
        });
};

const update = (req, res) => {
    const id = req.params.id;

    Books.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Book updated successfully."
                });
            } else {
                res.status(404).send({
                    message: `Cannot update Book with id=${id}. Maybe book was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: `Error updating book with id= ${id}`
            });
        });
};

const findAllByGenre = (req, res) => {
    const genre = req.params.id.toLowerCase();

    Books.findAll({ where: { genre: genre } })
        .then(data => {
            if (data.length > 0) {
                res.send(data);
            } else {
                res.status(404).send({
                    message: `No book found with genre: ${genre}`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving books."
            });
        });
};


module.exports = {
    create,
    findAll,
    findOne,
    update,
    findAllByGenre,
}