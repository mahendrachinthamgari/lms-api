const db = require("../models");
const Customers = db.customers;
const Books = db.books;


const findAll = (req, res) => {

    Customers.findAll({})
        .then(data => {
            if (data.length > 0) {
                res.send(data);
            } else {
                res.status(404).send({
                    message: "No Customers found"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving Customers data."
            });
        });
};

// one to many realation

const getUserBooks = (req, res) => {
    const id = req.params.id

    Customers.findOne({
        include: [{
            model: Books,
            as: 'books'
        }],
        where: { customerId: id }
    })
        .then((data) => {
            if (data) {
                res.send(data)
            } else {
                res.status(404).send({
                    message: "User doesn't exist"
                })
            }
        })
        .catch((err) => {
            res.status(500).send({
                message: "Error occured while retreiving the user"
            })
        })
}



module.exports = {
    findAll,
    getUserBooks,
}