const express = require("express");
const cors = require("cors");

const app = express();

const corsOptions = {
    origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

const db = require("./app/models");
db.sequelize.sync()
    .then(() => {
        console.log("Synced db.");
    })
    .catch((err) => {
        console.log(`Failed to sync db: ${err.message}`);
    });

app.get("/", (req, res) => {
    res.json({ message: "Welcome to Library Management System" });
});

require("./app/routes/books.routes")(app);
require("./app/routes/customers.routes")(app);
require("./app/routes/authors.routes")(app);

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});